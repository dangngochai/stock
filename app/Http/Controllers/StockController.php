<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Stock;

class StockController extends Controller
{
    public function index()
    {
        $stocks = Stock::all()->toArray();
        return array_reverse($stocks);      
    }

    public function store(Request $request)
    {
        $stock = new Stock([
            'symbol' => $request->input('symbol'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity')
        ]);
        $stock->save();

        return response()->json('Stock created!');
    }

    public function show($id)
    {
        $stock = Stock::find($id);
        return response()->json($stock);
    }

    public function update($id, Request $request)
    {
        $stock = Stock::find($id);
        $stock->update($request->all());

        return response()->json('Stock updated!');
    }

    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();

        return response()->json('Stock deleted!');
    }
}
