import AllStock from './components/AllStock.vue';
import CreateStock from './components/CreateStock.vue';
import EditStock from './components/EditStock.vue';
 
export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllStock
    },
    {
        name: 'create',
        path: '/create',
        component: CreateStock
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditStock
    }
];